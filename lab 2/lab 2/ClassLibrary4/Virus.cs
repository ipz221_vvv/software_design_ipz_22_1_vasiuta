﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary4
{
    public class Virus : IVirusPrototype
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public double Weight { get; set; }
        public string Type { get; set; }
        public Virus[] Children { get; set; }

        public Virus(string name, int age, double weight, string type, Virus[] children = null)
        {
            Name = name;
            Age = age;
            Weight = weight;
            Type = type;
            Children = children ?? new Virus[0];
        }

        public IVirusPrototype Clone()
        {
            // Клонуємо себе
            Virus clone = (Virus)this.MemberwiseClone();

            // Клонуємо дітей
            if (Children != null)
            {
                clone.Children = new Virus[Children.Length];
                for (int i = 0; i < Children.Length; i++)
                {
                    clone.Children[i] = (Virus)Children[i].Clone();
                }
            }

            return clone;
        }

        public override string ToString()
        {
            return $"Ім'я: {Name}, Вік: {Age}, Вага: {Weight}, Вид: {Type}, Кількість дітей: {Children.Length}";
        }
    }
}
