﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;

namespace task1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            ISubscriptionCreator websitePurchase = new WebSite();
            Subscription premiumSubscription = websitePurchase.CreateSubscription();
            premiumSubscription.DisplayInfo();

            ISubscriptionCreator mobileAppPurchase = new MobileApp();
            Subscription educationalSubscription = mobileAppPurchase.CreateSubscription();
            educationalSubscription.DisplayInfo();

            ISubscriptionCreator managerCallPurchase = new ManagerCall();
            Subscription domesticSubscription = managerCallPurchase.CreateSubscription();
            domesticSubscription.DisplayInfo();
        }
    }
}
