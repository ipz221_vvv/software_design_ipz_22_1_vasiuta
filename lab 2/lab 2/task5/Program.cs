﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary5;

namespace task5
{
    internal class Program
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;

            var director = new Director();

            var heroBuilder = new HeroBuilder();
            var hero = director.ConstructHero(heroBuilder);

            Console.WriteLine("Герой 0_0");
            Console.WriteLine(hero);

            var enemyBuilder = new EnemyBuilder();
            var enemy = director.ConstructEnemy(enemyBuilder);

            Console.WriteLine("\nЗлодій -_-");
            Console.WriteLine(enemy);
        }
    }
}
