﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{
    public class NetbookFactory : DeviceFactory
    {
        public override IDevice CreateDevice(string brand)
        {
            return new Netbook(brand);
        }
    }
}
