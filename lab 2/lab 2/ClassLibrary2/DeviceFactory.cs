﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{
    public abstract class DeviceFactory
    {
        public abstract IDevice CreateDevice(string brand);
    }
}
