﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{
    public class SmartphoneFactory : DeviceFactory
    {
        public override IDevice CreateDevice(string brand)
        {
            return new Smartphone(brand);
        }
    }
}
