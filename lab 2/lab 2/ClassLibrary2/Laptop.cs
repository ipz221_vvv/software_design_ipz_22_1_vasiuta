﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{
    public class Laptop : IDevice
    {
        private string _brand;

        public Laptop(string brand)
        {
            _brand = brand;
        }

        public string GetDeviceInfo()
        {
            return $"Laptop of brand {_brand}";
        }
    }
}
