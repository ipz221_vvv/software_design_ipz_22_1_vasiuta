﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary4;

namespace task4
{
    internal class Program
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;

            Virus grandChild1 = new Virus("Внучок1", 1, 0.1, "ТипC");
            Virus grandChild2 = new Virus("Внучок2", 1, 0.1, "ТипC");

            Virus child1 = new Virus("Дитина1", 5, 1.0, "ТипB", new[] { grandChild1 });
            Virus child2 = new Virus("Дитина2", 5, 1.2, "ТипB", new[] { grandChild2 });

            Virus parent = new Virus("Батько", 10, 2.0, "ТипA", new[] { child1, child2 });

            Virus clonedParent = (Virus)parent.Clone();

            Console.WriteLine("Оригінальний вірус:");
            PrintVirusHierarchy(parent);

            Console.WriteLine("\nКлонований вірус:");
            PrintVirusHierarchy(clonedParent);
        }

        static void PrintVirusHierarchy(Virus virus, int level = 0)
        {
            Console.WriteLine(new String(' ', level * 2) + virus);
            foreach (var child in virus.Children)
            {
                PrintVirusHierarchy(child, level + 1);
            }
        }
    }
}
