﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary5
{
    public class EnemyBuilder : ICharacterBuilder
    {
        private Character _character = new Character();
        private List<string> _evilDeeds = new List<string>();

        public ICharacterBuilder SetHairColor(string color)
        {
            _character.HairColor = color;
            return this;
        }

        public ICharacterBuilder SetEyeColor(string color)
        {
            _character.EyeColor = color;
            return this;
        }

        public ICharacterBuilder SetClothing(string clothing)
        {
            _character.Clothing = clothing;
            return this;
        }

        public ICharacterBuilder AddInventoryItem(string item)
        {
            _character.Inventory.Add(item);
            return this;
        }

        public ICharacterBuilder AddSpecialAbility(string ability)
        {
            return this;
        }

        public ICharacterBuilder AddEvilDeed(string deed)
        {
            _evilDeeds.Add(deed);
            return this;
        }

        public Character Build()
        {
            _character.EvilDeeds = _evilDeeds;
            return _character;
        }
    }
}
