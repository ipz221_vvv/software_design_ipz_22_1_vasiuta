﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary5
{
    public class Director
    {
        public Character ConstructHero(ICharacterBuilder builder)
        {
            return builder
                .SetHairColor("Русявий")
                .SetEyeColor("Блакитний")
                .SetClothing("Лицарські обладунки")
                .AddInventoryItem("Меч")
                .AddInventoryItem("Щит")
                .AddInventoryItem("Хілочка")
                .AddSpecialAbility("Убив дракона") 
                .AddSpecialAbility("Врятував принцесу")
                .Build();
        }

        public Character ConstructEnemy(ICharacterBuilder builder)
        {
            return builder
                .SetHairColor("Чорний")
                .SetEyeColor("Рубіновий")
                .SetClothing("Чорний плащ")
                .AddInventoryItem("Некрономікон")
                .AddInventoryItem("Темний посох")
                .AddEvilDeed("Знищення сіл")
                .AddEvilDeed("Грабунок караванів")
                .Build();
        }
    }
}
