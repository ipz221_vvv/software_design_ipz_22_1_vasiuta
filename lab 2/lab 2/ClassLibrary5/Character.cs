﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary5
{
    public class Character
    {
        public string HairColor { get; set; }
        public string EyeColor { get; set; }
        public string Clothing { get; set; }
        public List<string> Inventory { get; set; } = new List<string>();
        public List<string> Feats { get; set; } = new List<string>();
        public List<string> EvilDeeds { get; set; } = new List<string>();

        public override string ToString()
        {
            var featsOutput = Feats.Count > 0 ? $"Подвиги: {string.Join(", ", Feats)}" : "";
            var evilDeedsOutput = EvilDeeds.Count > 0 ? $"Злодіяння: {string.Join(", ", EvilDeeds)}" : "";

            return $"Колір волосся: {HairColor}\n" +
                   $"Колір очей: {EyeColor}\n" +
                   $"Одяг: {Clothing}\n" +
                   $"Інвентар: {string.Join(", ", Inventory)}\n" +
                   $"{featsOutput}" +
                   $"{evilDeedsOutput}";
        }
    }
}
