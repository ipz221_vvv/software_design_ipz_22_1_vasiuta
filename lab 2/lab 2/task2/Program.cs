﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary2;

namespace task2
{
    internal class Program
    {
        static void Main()
        {
            DeviceFactory laptopFactory = new LaptopFactory();
            DeviceFactory netbookFactory = new NetbookFactory();
            DeviceFactory ebookFactory = new EBookFactory();
            DeviceFactory smartphoneFactory = new SmartphoneFactory();

            IDevice myLaptop = laptopFactory.CreateDevice("IProne");
            IDevice myNetbook = netbookFactory.CreateDevice("Kiaomi");
            IDevice myEBook = ebookFactory.CreateDevice("Balaxy");
            IDevice mySmartphone = smartphoneFactory.CreateDevice("IProne");

            Console.WriteLine(myLaptop.GetDeviceInfo());
            Console.WriteLine(myNetbook.GetDeviceInfo());
            Console.WriteLine(myEBook.GetDeviceInfo());
            Console.WriteLine(mySmartphone.GetDeviceInfo());
        }
    }
}
