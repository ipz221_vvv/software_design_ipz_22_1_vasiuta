﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class DomesticSubscription : Subscription
{
    public DomesticSubscription() : base(29.99m, 6, new List<string> { "Новини", "Спорт", "Розваги" }, new List<string> { "HD Якість", "5 Пристроїв" })
    {
    }

    public override void DisplayInfo()
    {
        Console.WriteLine("Домашня підписка");
        Console.WriteLine($"Щомісячна плата: {MonthlyFee} грн");
        Console.WriteLine($"Мінімальний період: {MinimumSubscriptionPeriod} місяців");
        Console.WriteLine($"Канали: {string.Join(", ", Channels)}");
        Console.WriteLine($"Переваги: {string.Join(", ", Features)}");
        Console.WriteLine();
    }
}

