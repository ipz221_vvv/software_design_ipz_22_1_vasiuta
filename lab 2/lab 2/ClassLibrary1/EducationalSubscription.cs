﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class EducationalSubscription : Subscription
{
    public EducationalSubscription() : base(19.99m, 12, new List<string> { "Освітні", "Документальні" }, new List<string> { "4K Якість", "2 Пристрої" })
    {
    }

    public override void DisplayInfo()
    {
        Console.WriteLine("Освітня підписка");
        Console.WriteLine($"Щомісячна плата: {MonthlyFee} грн");
        Console.WriteLine($"Мінімальний період: {MinimumSubscriptionPeriod} місяців");
        Console.WriteLine($"Канали: {string.Join(", ", Channels)}");
        Console.WriteLine($"Переваги: {string.Join(", ", Features)}");
        Console.WriteLine();
    }
}

