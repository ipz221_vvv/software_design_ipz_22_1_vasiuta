﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public abstract class Subscription
{
    public decimal MonthlyFee { get; protected set; }
    public int MinimumSubscriptionPeriod { get; protected set; }
    public List<string> Channels { get; protected set; }
    public List<string> Features { get; protected set; }

    protected Subscription(decimal monthlyFee, int minimumSubscriptionPeriod, List<string> channels, List<string> features)
    {
        MonthlyFee = monthlyFee;
        MinimumSubscriptionPeriod = minimumSubscriptionPeriod;
        Channels = channels;
        Features = features;
    }

    public abstract void DisplayInfo();
}
