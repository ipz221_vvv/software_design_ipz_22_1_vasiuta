﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class PremiumSubscription : Subscription
{
    public PremiumSubscription() : base(49.99m, 3, new List<string> { "Всі канали" }, new List<string> { "4K Якість", "Необмежені пристрої", "Без реклами" })
    {
    }

    public override void DisplayInfo()
    {
        Console.WriteLine("Преміум підписка");
        Console.WriteLine($"Щомісячна плата: {MonthlyFee} грн");
        Console.WriteLine($"Мінімальний період: {MinimumSubscriptionPeriod} місяців");
        Console.WriteLine($"Канали: {string.Join(", ", Channels)}");
        Console.WriteLine($"Переваги: {string.Join(", ", Features)}");
        Console.WriteLine();
    }
}
