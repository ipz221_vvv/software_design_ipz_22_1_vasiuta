﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace task3
{
    public class Authenticator
    {
        private static Authenticator _instance;
        private static readonly object _lock = new object();

        private Authenticator()
        {

        }

        public static Authenticator Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_lock)
                    {
                        if (_instance == null)
                        {
                            _instance = new Authenticator();
                        }
                    }
                }
                return _instance;
            }
        }

        public void Authenticate()
        {
            Console.WriteLine("Аутентифікація триває...");
        }
    }

    class Program
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;

            Thread t1 = new Thread(() =>
            {
                var auth1 = Authenticator.Instance;
                auth1.Authenticate();
            });

            Thread t2 = new Thread(() =>
            {
                var auth2 = Authenticator.Instance;
                auth2.Authenticate();
            });

            t1.Start();
            t2.Start();

            t1.Join();
            t2.Join();

            var instance1 = Authenticator.Instance;
            var instance2 = Authenticator.Instance;

            Console.WriteLine($"Чи існує лише один екземпляр цього класу? {ReferenceEquals(instance1, instance2)}");
        }
    }
}
