﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{   
    public class Product
    {
        public string Name { get; set; }
        public Money Price { get; private set; }

        public Product(string name, Money price)
        {
            Name = name;
            Price = price;
        }

        public void SetPrice(Money price)
        {
            Price.Coins = price.Coins;
        }

        public void Display()
        {
            Console.WriteLine($"Товар: {Name}, Ціна: {Price.MoneyString()} грн");
            Console.WriteLine("-----------------------------");
        }
    }
}
