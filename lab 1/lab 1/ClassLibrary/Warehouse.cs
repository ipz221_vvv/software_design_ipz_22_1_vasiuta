﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Warehouse
    {
        public List<WarehouseItem> storage = new List<WarehouseItem>();

        public void AddItemToWarehouse(WarehouseItem newItem) 
        {
            this.storage.Add(newItem);
        }
    }
}
