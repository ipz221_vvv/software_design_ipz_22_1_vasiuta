﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Bill
    {
        public List<BillItem> billItems = new List<BillItem>();

        public void AddItemToBill(BillItem newItem)
        {
            this.billItems.Add(newItem);
        }

        public void DisplayWaybill()
        {
            Console.WriteLine("Надрукований чек:\n");

            decimal billTotal = 0;

            foreach (BillItem billitem in billItems)
            {
                Console.WriteLine($"Hазва: {billitem.Name}\n" +
                $"Кількість: {billitem.Quantity}\n" +
                $"Вартість: {billitem.Price.MoneyString()} грн\n" +
                $"За {billitem.Name}: {billitem.Price * billitem.Quantity} грн");
                Console.WriteLine("-----------------------------");
                billTotal += billitem.Price * billitem.Quantity;
            }

            Console.WriteLine($"\nДо сплати: {billTotal} грн");
        }

    }
}
