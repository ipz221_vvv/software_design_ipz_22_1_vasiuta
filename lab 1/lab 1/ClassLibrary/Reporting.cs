﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ClassLibrary
{
    public class Reporting
    {
        public Warehouse warehouse;
        public Bill bill;

        public Reporting(Warehouse warehouse, Bill bill)
        {
            this.warehouse = warehouse;
            this.bill = bill;   
        }

        public void DisplayInventory() 
        {
            Console.WriteLine("Інвентаризація:\n");

            foreach (WarehouseItem item in warehouse.storage)
            {
                Console.WriteLine($"Назва: {item.Name}\n" +
                    $"Ціна: {item.Price.MoneyString()} грн\n" +
                    $"Ціна закупку: {item.PurchasePrice.MoneyString()} грн\n" +
                    $"Одиниця виміру: {item.Unit}\n" +
                    $"Кількість: {item.Quantity}\n" +
                    $"Останнє поповерння: {item.LastArrivalDate:dd.MM.yyyy}");
                Console.WriteLine("-----------------------------");
            }
        }

        public void DisplayProfit()
        {
            Console.WriteLine("Прибуткова накладна:\n");

            foreach (WarehouseItem item in warehouse.storage)
            {
                Console.WriteLine($"назва: {item.Name}\n" +
                $"прибуток: {(item.Price - item.PurchasePrice) * item.Quantity} грн");
                Console.WriteLine("-----------------------------");

            }
        }

    }
}
