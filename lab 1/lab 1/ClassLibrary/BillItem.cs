﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class BillItem : Product
    {
        public decimal Quantity { get; set; }

        public BillItem(Product product, decimal Quantity) : base(product.Name, product.Price)
        {
            this.Quantity = Quantity;
        }

    }
}
