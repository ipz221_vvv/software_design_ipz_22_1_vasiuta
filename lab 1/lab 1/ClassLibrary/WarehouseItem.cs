﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class WarehouseItem : Product
    {
        public string Unit { get; set; }
        public Money PurchasePrice { get; private set; }
        public decimal Quantity { get; set; }
        public DateTime LastArrivalDate { get; set; }

        public WarehouseItem(Product product, string unit, Money purchasePrice, decimal quantity, DateTime lastArrivalDate) : base(product.Name, product.Price)
        {
            Unit = unit;
            PurchasePrice = purchasePrice;
            Quantity = quantity;
            LastArrivalDate = lastArrivalDate;
        }
    }
}
