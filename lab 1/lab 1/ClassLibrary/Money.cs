﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ClassLibrary
{
    public class Money
    {
        public int WholePart
        {
            get
            {
                return Coins / 100;
            }
            set {;}
        }
        public int FractionalPart 
        {
            get 
            {
                return Coins % 100;
            }
            set {;}
        }
        public int Coins { get; set; }

        public Money(int wholePart, int fractionalPart)
        {
            WholePart = wholePart;
            FractionalPart = fractionalPart;

            Coins = wholePart * 100 + fractionalPart;
        }

        public string MoneyString()  
        {
            return Convert.ToString(Convert.ToDouble(Coins) / 100);
        }

        public static Money operator -(Money instance1, Money instance2) 
        {
            int result = instance1.Coins - instance2.Coins;
            int wholePart = result / 100; 
            int fractionalPart = result % 100;
            return new Money(wholePart, fractionalPart);
        }

        public static decimal operator *(Money instance, decimal quantity)
        {
            return instance.Coins * quantity / 100;
        }

    }
}
