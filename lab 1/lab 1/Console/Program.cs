﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consoler
{
    internal class Program
    {
        static void Main()
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            Money priceApple = new Money(1, 05);
            Money priceMilk = new Money(3, 33);

            Product apple = new Product("Яблука", priceApple);
            Product milk = new Product("Молоко", priceMilk);

            Console.WriteLine("Інформація про товар перед зміною ціни:\n");

            apple.Display(); 
            milk.Display();

            Console.WriteLine("Інформація про товари після зміни ціни:\n");

            apple.SetPrice(priceApple - new Money(1, 0));
            milk.SetPrice(priceMilk - new Money(1, 0));

            apple.Display();
            milk.Display();

            Warehouse storage = new Warehouse();

            WarehouseItem itemApple = new WarehouseItem(apple, "kg", new Money(2, 0), 100, DateTime.Today);
            WarehouseItem itemMilk = new WarehouseItem(milk, "l", new Money(1, 0), 20, DateTime.Today);

            storage.AddItemToWarehouse(itemApple);
            storage.AddItemToWarehouse(itemMilk);

            Bill bill = new Bill();

            BillItem billItemApple = new BillItem(apple, 30);
            BillItem billItemMilk = new BillItem(milk, 20);

            bill.AddItemToBill(billItemApple);
            bill.AddItemToBill(billItemMilk);

            Reporting reporting = new Reporting(storage, bill);

            reporting.DisplayInventory();
            reporting.DisplayProfit();
            bill.DisplayWaybill();


            Console.ReadKey();
        }
    }
}
