# Programming Principles in the Lab Work

This document explains how various programming principles are adhered to in the provided code. Each principle is demonstrated with relevant code snippets and file references.

## Principles Demonstrated

### 1. DRY (Don't Repeat Yourself)
The DRY principle is followed by encapsulating repeated logic in functions and classes. For example:
- The `Money` class encapsulates currency-related operations, preventing repetition of similar logic throughout the code.
  - **File**: [Money.cs](ClassLibrary/Money.cs#L5-L15)

### 2. KISS (Keep It Simple, Stupid)
The KISS principle is maintained by keeping methods simple and focused on a single task. For instance:
- The `Product` class has simple methods for setting prices and displaying product information.
  - **File**: [Product.cs](ClassLibrary/Product.cs#L16-L25)

### 3. SOLID Principles
#### Single Responsibility Principle (SRP)
Each class has a single responsibility. For example:
- The `Product` class manages product details.
  - **File**: [Product.cs](ClassLibrary/Product.cs#L5-L10)

#### Open/Closed Principle (OCP)
Classes are open for extension but closed for modification. For example:
- New types of products can be added without changing the existing `Product` class.
  - **File**: [Product.cs](ClassLibrary/Product.cs)

#### Liskov Substitution Principle (LSP)
Subtypes can replace their base types. For example:
- `BillItem` extends `Product` and can be used wherever `Product` is expected.
  - **File**: [BillItem.cs](ClassLibrary/BillItem.cs#L7-L10)

#### Interface Segregation Principle (ISP)
Interfaces are not used in this code, but methods are designed to avoid unnecessary dependencies.

#### Dependency Inversion Principle (DIP)
Higher-level modules depend on abstractions. For example:
- The `Reporting` class depends on the `Warehouse` and `Bill` abstractions.
  - **File**: [Reporting.cs](ClassLibrary/Reporting.cs#L8-L13)

### 4. YAGNI (You Aren't Gonna Need It)
The code avoids adding functionality until it is necessary. For example:
- Only required methods and properties are added to the classes.
  - **File**: [Product.cs](ClassLibrary/Product.cs#L5-L15)

### 5. Composition Over Inheritance
Composition is favored over inheritance to achieve code reuse. For example:
- The `WarehouseItem` class composes `Product` instead of inheriting from it.
  - **File**: [WarehouseItem.cs](ClassLibrary/WarehouseItem.cs#L12-L15)

### 6. Program to Interfaces not Implementations
Interfaces are not explicitly used, but classes are designed to interact through abstractions. For example:
- The `Reporting` class interacts with `Warehouse` and `Bill` without depending on their implementations.
  - **File**: [Reporting.cs](ClassLibrary/Reporting.cs#L8-L13)

### 7. Fail Fast
The code ensures errors are detected early. For example:
- Constructor validation ensures that `Money` instances are created with valid values.
  - **File**: [Money.cs](ClassLibrary/Money.cs#L18-L21)

## References
- [Class Program](Consoler/Program.cs)
- [Class BillItem](ClassLibrary/BillItem.cs)
- [Class Bill](ClassLibrary/Bill.cs)
- [Class Warehouse](ClassLibrary/Warehouse.cs)
- [Class Reporting](ClassLibrary/Reporting.cs)
- [Class WarehouseItem](ClassLibrary/WarehouseItem.cs)
- [Class Product](ClassLibrary/Product.cs)
- [Class Money](ClassLibrary/Money.cs)
