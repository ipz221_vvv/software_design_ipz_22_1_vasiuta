﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary3;

namespace task3
{
    internal class Program
    {

        static void Main()
        {
            Renderer vectorRenderer = new VectorRenderer();
            Renderer rasterRenderer = new RasterRenderer();

            Shape circle = new Circle(vectorRenderer);
            Shape square = new Square(rasterRenderer);
            Shape triangle = new Triangle(vectorRenderer);

            circle.Draw();
            square.Draw();
            triangle.Draw();
        }
    }
}
