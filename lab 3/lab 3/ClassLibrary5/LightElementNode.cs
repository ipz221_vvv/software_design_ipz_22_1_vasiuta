﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary5
{
    public class LightElementNode : LightNode
    {
        private string _tagName;
        private string _displayType;
        private string _closingType;
        private List<string> _cssClasses;
        private List<LightNode> _children;

        public LightElementNode(string tagName, string displayType, string closingType)
        {
            _tagName = tagName;
            _displayType = displayType;
            _closingType = closingType;
            _cssClasses = new List<string>();
            _children = new List<LightNode>();
        }

        public void AddCssClass(string cssClass)
        {
            _cssClasses.Add(cssClass);
        }

        public void AddChild(LightNode child)
        {
            _children.Add(child);
        }

        public override string OuterHTML
        {
            get
            {
                string html = $"<{_tagName}";

                if (_cssClasses.Count > 0)
                {
                    html += $" class=\"{string.Join(" ", _cssClasses)}\"";
                }

                if (_closingType == "single")
                {
                    html += " />";
                }
                else
                {
                    html += ">";

                    foreach (var child in _children)
                    {
                        html += Environment.NewLine + "" + child.OuterHTML;
                    }

                    html += Environment.NewLine + "</" + _tagName + ">";
                }

                return html;
            }
        }

        public override string InnerHTML
        {
            get
            {
                string html = "";

                foreach (var child in _children)
                {
                    html += child.OuterHTML;
                }

                return html;
            }
        }
    }
}
