﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ClassLibrary2;

namespace task2
{
    internal class Program
    {
        static void Main()
        {
            Hero warrior = new Warrior("Aragorn");
            Hero mage = new Mage("Gandalf");
            Hero paladin = new Paladin("Uther");

            warrior.Inventory.Add(new Armor());
            warrior.Inventory.Add(new Weapon());

            mage.Inventory.Add(new Artifact());
            mage.Inventory.Add(new Weapon());

            paladin.Inventory.Add(new Armor());
            paladin.Inventory.Add(new Artifact());

            warrior.PrintInfo();
            mage.PrintInfo();
            paladin.PrintInfo();
        }
    }
}
