﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using ClassLibrary1;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            Logger consoleLogger = new Logger();
            consoleLogger.Log("Це інформаційне повідомлення.");
            consoleLogger.Error("Це повідомлення про помилку.");
            consoleLogger.Warn("Це попередження.");

            FileWriter fileWriter = new FileWriter("log.txt");
            Logger fileLogger = new FileLoggerAdapter(fileWriter);
            fileLogger.Log("Це інформаційне повідомлення у файлі.");
            fileLogger.Error("Це повідомлення про помилку у файлі.");
            fileLogger.Warn("Це попередження у файлі.");

            Console.WriteLine("Логування завершено.");
        }
    }
}
