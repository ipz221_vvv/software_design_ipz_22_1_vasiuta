﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{
    public abstract class InventoryDecorator
    {
        public abstract string Description { get; }
    }

    public class Armor : InventoryDecorator
    {
        public override string Description => "Armor";
    }

    public class Weapon : InventoryDecorator
    {
        public override string Description => "Weapon";
    }

    public class Artifact : InventoryDecorator
    {
        public override string Description => "Artifact";
    }
}
