﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{
    public abstract class Hero
    {
        public string Name { get; }
        public List<InventoryDecorator> Inventory { get; } = new List<InventoryDecorator>();

        protected Hero(string name)
        {
            Name = name;
        }

        public virtual string GetInfo()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Name - {Name}");
            sb.AppendLine("Inventory:");

            if (Inventory.Count == 0)
            {
                sb.AppendLine("  None");
            }
            else
            {
                foreach (var item in Inventory)
                {
                    sb.AppendLine($"  - {item.Description}");
                }
            }

            return sb.ToString();
        }

        public void PrintInfo()
        {
            Console.WriteLine(GetInfo());
        }
    }

    public class Warrior : Hero
    {
        public Warrior(string name) : base(name) { }

        public override string GetInfo()
        {
            return $"Warrior\n{base.GetInfo()}";
        }
    }

    public class Mage : Hero
    {
        public Mage(string name) : base(name) { }

        public override string GetInfo()
        {
            return $"Mage\n{base.GetInfo()}";
        }
    }

    public class Paladin : Hero
    {
        public Paladin(string name) : base(name) { }

        public override string GetInfo()
        {
            return $"Paladin\n{base.GetInfo()}";
        }
    }
}
