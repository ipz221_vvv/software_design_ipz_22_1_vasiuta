﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary5;

namespace task5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            var table = new LightElementNode("table", "block", "double");
            table.AddCssClass("table");

            for (int i = 0; i < 3; i++)
            {
                var row = new LightElementNode("tr", "block", "double");
                table.AddChild(row);

                for (int j = 0; j < 3; j++)
                {
                    var cell = new LightElementNode("td", "block", "double");
                    row.AddChild(cell);

                    var text = new LightTextNode($"Клітинка {i + 1},{j + 1}");
                    cell.AddChild(text);
                }
            }

            Console.WriteLine(table.OuterHTML);
        }
    }
}
