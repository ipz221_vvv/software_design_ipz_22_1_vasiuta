﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace ClassLibrary4
{
    public class SmartTextReader
    {
        private string _filePath;

        public SmartTextReader(string filePath)
        {
            _filePath = filePath;
        }

        public char[][] ReadText()
        {
            if (!File.Exists(_filePath))
            {
                throw new FileNotFoundException("File not found", _filePath);
            }

            var lines = File.ReadAllLines(_filePath);
            var result = new char[lines.Length][];

            for (int i = 0; i < lines.Length; i++)
            {
                result[i] = lines[i].ToCharArray();
            }

            return result;
        }
    }
}
