﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary4
{
    public class SmartTextChecker
    {
        private SmartTextReader _smartTextReader;

        public SmartTextChecker(SmartTextReader smartTextReader)
        {
            _smartTextReader = smartTextReader;
        }

        public char[][] ReadText()
        {
            try
            {
                Console.WriteLine("Opening file...");
                char[][] content = _smartTextReader.ReadText();
                Console.WriteLine("File read successfully.");

                int lineCount = content.Length;
                int charCount = 0;
                foreach (var line in content)
                {
                    charCount += line.Length;
                }

                Console.WriteLine($"Total lines: {lineCount}");
                Console.WriteLine($"Total characters: {charCount}");

                Console.WriteLine("\nFile Content:");
                for (int i = 0; i < content.Length; i++)
                {
                    Console.WriteLine($"Line {i + 1}: {new string(content[i])}");
                }

                return content;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                return null;
            }
            finally
            {
                Console.WriteLine("Closing file...");
            }
        }
    }
}
