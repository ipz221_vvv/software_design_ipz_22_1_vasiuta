﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ClassLibrary4
{
    public class SmartTextReaderLocker
    {
        private SmartTextReader _smartTextReader;
        private Regex _fileAccessRegex;

        public SmartTextReaderLocker(SmartTextReader smartTextReader, string fileAccessPattern)
        {
            _smartTextReader = smartTextReader;
            _fileAccessRegex = new Regex(fileAccessPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
        }

        public char[][] ReadText()
        {
            string filePath = _smartTextReader.GetType().GetField("_filePath", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(_smartTextReader) as string;

            if (_fileAccessRegex.IsMatch(filePath))
            {
                Console.WriteLine("Access denied!");
                return null;
            }
            else
            {
                return _smartTextReader.ReadText();
            }
        }
    }
}
