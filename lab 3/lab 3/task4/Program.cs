﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary4;
using System.IO;

namespace task4
{
    internal class Program
    {
        static void Main()
        {
            string filePath = "example.txt";

            SmartTextReader reader = new SmartTextReader(filePath);
            SmartTextChecker checker = new SmartTextChecker(reader);
            SmartTextReaderLocker locker = new SmartTextReaderLocker(reader, ".*restricted.*");

            File.WriteAllText(filePath, "Hello World\nThis is a test\nAccess denied file");

            checker.ReadText();

            char[][] result = locker.ReadText();

            SmartTextReaderLocker lockerNonRestricted = new SmartTextReaderLocker(reader, "nonexistentpattern");
            result = lockerNonRestricted.ReadText();
        }
    }
}
